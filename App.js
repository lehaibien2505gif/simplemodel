import React, { useState, useEffect, Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  SafeAreaView,
  TouchableOpacity,
  Modal
} from 'react-native';

import { SimpleModal } from './components/SimpleModal.js';

const App = () => {

  const [ModalVisible, setModalVidaVisible] = useState(false);

  const [chooseData, setchooseData] = useState();

  const changeModalVisible = (bool) => {
    setModalVidaVisible(bool)
  }

  const setData = (data) => { setchooseData(data); }

  return (
    <SafeAreaView style={styles.container}>

      <View>
        <Text style={styles.text}>
          {chooseData}
        </Text>
      </View>


      <TouchableOpacity
        style={styles.touchableOpacity}
        onPress={() => changeModalVisible(true)}
      >

        <Text style={styles.text}>Open Modal</Text>

      </TouchableOpacity>

      <Modal
        transparent={true}
        animationType='fade'
        visible={ModalVisible}
        nRequestClose={() => changeModalVisible(false)}
      >

        <SimpleModal
          changeModalVisible={changeModalVisible}
          setData={setData} />

      </Modal>

    </SafeAreaView >
  );

};

const styles = StyleSheet.create({

  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#367E18',

  },

  touchableOpacity: {
    backgroundColor: '#F57328',
    paddingHorizontal: 50,
    borderRadius: 10,

  },

  text: {
    marginVertical: 20,
    fontSize: 20,
    fontWeight: '500',
    color: '#FFE9A0'
  },


});

export default App;