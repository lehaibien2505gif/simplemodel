import React from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Dimensions,
    Modal
} from 'react-native';

const HEIGHT_MODAL = 150;
const WIDTH = Dimensions.get('window').width;


const SimpleModal = (props) => {

    const closeModal = (bool, data) => {
        props.changeModalVisible(bool);
        props.setData(data);
    };

    return (
        <TouchableOpacity
            disabled={true}
            style={styles.container}>

            <View style={styles.modal}>

                <View style={styles.textView}>
                    <Text style={styles.text}>Sample Modal Header</Text>
                    <Text style={styles.text}>Sample Modal Description</Text>
                </View>

                <View style={styles.btnView}>
                    <TouchableOpacity
                        style={styles.touchableOpacity}
                        onPress={() => closeModal(false, 'Cancel')}
                    >

                        <Text style={[styles.text, { color: 'black', fontWeight: '400' }]}>CANCEL</Text>

                    </TouchableOpacity>

                    <TouchableOpacity
                        style={styles.touchableOpacity}
                        onPress={() => closeModal(false, 'Ok')}
                    >

                        <Text style={[styles.text, { color: '#1363DF', fontWeight: '500' }]}>OK</Text>
                    </TouchableOpacity>
                </View>

            </View>
        </TouchableOpacity>
    );
}

const styles = StyleSheet.create({
    modal: {
        height: HEIGHT_MODAL,
        width: WIDTH - 80,
        paddingTop: 10,
        backgroundColor: 'white',
        borderRadius: 10
    },

    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },

    text: {
        margin: 5,
        fontSize: 16,
        fontWeight: '700',
        color: '#181818'

    },

    touchableOpacity: {
        flex: 1,
        paddingVertical: 10,
        alignItems: 'center'

    },

    textView: {
        flex: 1,
        alignItems: 'center'
    },

    btnView: {
        width: '100%',
        flexDirection: 'row'
    }
});

export { SimpleModal }